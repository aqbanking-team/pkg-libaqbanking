#! /bin/sh

echoexec() {
  echo $@
  $@
}

compile_execute_test() {
  local name=${0##*/}
  local package="$@"

  CFLAGS=$(pkg-config --cflags $package)
  LIBS=$(pkg-config --libs $package)

  echo "Using CFLAGS: $CFLAGS"
  echo "Using LIBS:   $LIBS"
  echo
  echo "Compiling ${name}.c"
  echoexec gcc -c -o $AUTOPKGTEST_TMP/${name}.o debian/tests/src/${name}.c $CFLAGS
  echo
  echo "Linking ${name}"
  echoexec gcc -o $AUTOPKGTEST_TMP/$name $AUTOPKGTEST_TMP/${name}.o $LIBS
  echo
  set -x
  mkdir -pv $AUTOPKGTEST_TMP/home
  export HOME=$AUTOPKGTEST_TMP/home
  chmod a+x $AUTOPKGTEST_TMP/$name
  set +x
  echo "Execute built binary: ${name}"
  echoexec $AUTOPKGTEST_TMP/$name
}
