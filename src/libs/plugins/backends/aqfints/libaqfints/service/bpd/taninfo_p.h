/**********************************************************
 * This file has been automatically created by "typemaker2"
 * from the file "taninfo.xml".
 * Please do not edit this file, all changes will be lost.
 * Better edit the mentioned source file instead.
 **********************************************************/

#ifndef AQFINTS_TANINFO_TANINFO_P_H
#define AQFINTS_TANINFO_TANINFO_P_H

#include "./taninfo.h"


#ifdef __cplusplus
extern "C" {
#endif

struct AQFINTS_TANINFO {
  GWEN_LIST_ELEMENT(AQFINTS_TANINFO)
  int _refCount;
  int jobsPerMsg;
  int minSigs;
  int securityClass;
  int minPinLen;
  int maxPinLen;
  int maxTanLen;
  char *userIdText;
  char *customerIdText;
  AQFINTS_TANJOBINFO_LIST *tanJobInfoList;
  uint32_t runtimeFlags; /* volatile */
};

#ifdef __cplusplus
}
#endif

#endif

